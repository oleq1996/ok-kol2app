import { Component, OnInit } from '@angular/core';
import {DataService} from "../../services/data.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'orders-details-ok',
  templateUrl: './orders-details-ok.component.html',
  styleUrls: ['./orders-details-ok.component.css']
})
export class OrdersDetailsOKComponent implements OnInit {

  public title: string = '';
  image: string = '';
  public text: string = '';

  constructor(private service:DataService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    let id: string ='';
    this.route.paramMap.subscribe((params:any)=>{
      id=params.get('id');
    });

    this.service.getById(id).subscribe((res:any)=>{
      this.title = res['title'];
      this.image = res['image'];
      this.text = res['text'];
    });
  }

}
