import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersDetailsOKComponent } from './orders-details-ok.component';

describe('OrdersDetailsOKComponent', () => {
  let component: OrdersDetailsOKComponent;
  let fixture: ComponentFixture<OrdersDetailsOKComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdersDetailsOKComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersDetailsOKComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
