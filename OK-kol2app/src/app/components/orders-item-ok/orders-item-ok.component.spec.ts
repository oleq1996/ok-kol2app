import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersItemOKComponent } from './orders-item-ok.component';

describe('OrdersItemOKComponent', () => {
  let component: OrdersItemOKComponent;
  let fixture: ComponentFixture<OrdersItemOKComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdersItemOKComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersItemOKComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
