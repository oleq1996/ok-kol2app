import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'orders-item-ok',
  templateUrl: './orders-item-ok.component.html',
  styleUrls: ['./orders-item-ok.component.css']
})
export class OrdersItemOKComponent implements OnInit {

  @Input() id?:number;
  @Input() title?:string;
  @Input() text?:string;
  @Input() image?:string

  constructor() { }

  ngOnInit(): void {
  }

}
