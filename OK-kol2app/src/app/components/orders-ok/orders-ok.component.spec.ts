import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersOKComponent } from './orders-ok.component';

describe('OrdersOKComponent', () => {
  let component: OrdersOKComponent;
  let fixture: ComponentFixture<OrdersOKComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdersOKComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersOKComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
