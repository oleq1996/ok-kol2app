import { Component, OnInit } from '@angular/core';
import {DataService} from "../../services/data.service";

@Component({
  selector: 'orders-ok',
  templateUrl: './orders-ok.component.html',
  styleUrls: ['./orders-ok.component.css']
})
export class OrdersOKComponent implements OnInit {

  public items$: any;
  constructor(private service: DataService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(){
    this.service.getAll().subscribe(response=>{
      this.items$ = response;
    });
  }
}
