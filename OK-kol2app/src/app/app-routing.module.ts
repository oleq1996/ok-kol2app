import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {OrdersOKComponent} from "./components/orders-ok/orders-ok.component";
import {OrdersDetailsOKComponent} from "./components/orders-details-ok/orders-details-ok.component";

const routes: Routes = [
  {
    path: 'orders/detail/:id',
    component:OrdersDetailsOKComponent
  },
  {
    path: 'orders',
    component: OrdersOKComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
