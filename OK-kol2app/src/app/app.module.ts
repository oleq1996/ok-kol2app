import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OrdersOKComponent } from './components/orders-ok/orders-ok.component';
import { OrdersItemOKComponent } from './components/orders-item-ok/orders-item-ok.component';
import { OrdersDetailsOKComponent } from './components/orders-details-ok/orders-details-ok.component';
import {DataService} from "./services/data.service";
import {HttpClient, HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    OrdersOKComponent,
    OrdersItemOKComponent,
    OrdersDetailsOKComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
